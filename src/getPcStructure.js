import fetch from 'isomorphic-fetch';

const url = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

export default async function getPcStructure(){

	let pc = {};

	try{

		const response = await fetch(url);
				
		pc = await response.json();

	}catch(err){

		console.log('Ошибка при попытке прочитать структуру модели', err);

	};

	return pc;

};