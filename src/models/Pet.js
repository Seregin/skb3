import mongoose from 'mongoose';

import _ from 'lodash';

const { Schema } = mongoose;

const PetSchema = new Schema({
	
	type: {
		type: String,
		enum: ['cat', 'dog']		
	},

	name: {
		type: String,
		required: true,
	},

	owner: {

		type: Schema.Types.ObjectId,
		
		ref: 'User'

	}

},{

	timestamps: true,

});

PetSchema.methods.toJSON = function() {

	return _.pick(this, ['name', 'type', 'owner']);	

};

// PetSchema.methods.toObject = function() {

// 	return _.pick(this, ['name']);	

// };

export default mongoose.model('Pet', PetSchema, );