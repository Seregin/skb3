import express from 'express';

import cors from 'cors';

import mongoose from 'mongoose';

import Promise from  'bluebird'; // функция Promise.all

import bodyParser from 'body-parser';

import _ from 'lodash';

import saveDataInDb from './saveDataInDb';

import getPcStructure from './getPcStructure';

import Pet from './models/Pet';

import User from './models/User';

import isAdmin from './middlewares/isAdmin';

mongoose.Promise = Promise;

mongoose.connect('mongodb://publicdb.mgbeta.ru/andreyseregin_skb3');

const app = express();

app.use(bodyParser.json());

app.use(cors());

// app.use(isAdmin); // если подключить тут то будут проверятся все действия, поставим только в '/clear'

app.get('/clear', isAdmin, async (req, res) => {

	await User.remove({});

	await Pet.remove({});

	return res.send('clear complete');

});

app.get('/users', async (req, res) => {

	const users = await User.find();

	return res.json(users);

});

app.get('/pets', async (req, res) => {
	
	const pets = await Pet.find().populate('owner');

	return res.json(pets);

});

app.post('/data', async (req, res) => {

	const data = req.body;

	if (!data.user) return res.status(400).send('user required')

	if (!data.pets) data.pets = [];

	// Проверка на то что пользователь существует

	const user = await User.findOne({ name: data.user.name });

	if (user) return res.status(400).send('user.name is exists')

	//..

	try{

		const result = await saveDataInDb(data);

		return res.json(result);

	} catch(err) {

		return res.status(500).json(err);

	}

});

app.get('/task3A*', async (req, res) =>{{

	const url = req.url.toLowerCase();	

	const urlarr = url.split('/'); urlarr.splice(0, 1);

	const pc = await getPcStructure();

	if (urlarr[0] != 'task3a'){
	
		return res.status(404).send('Not Found');

	} else if (urlarr.length == 1){

		return res.status(200).json(pc);

	} else if (urlarr[1] == 'volumes'){

		try{

			const hdd = pc.hdd;

			const volums = {};

			hdd.forEach((item, i, arr) => {

				const name = item.volume;

				const size = item.size;

				if (!volums[name])  volums[name] = 0;  	

				volums[name] += size;					

			});		

			return res.status(200).json(volums);		

		}catch(err){

			console.log('Ошибка при попытке подсчитать место на дисках', err);

			return res.status(404).send('Not Found'); 

		};


	} else {

		urlarr.splice(0, 1);

		let result = pc;

		urlarr.forEach((item, i, arr) => {

			result = result[item];

			if (!result) return res.status(404).send('Not Found'); 

		});

		return res.status(200).json(result);

	}

}});

app.listen(3000, function () {

 	console.log('Example app listening on port 3000!');

});
